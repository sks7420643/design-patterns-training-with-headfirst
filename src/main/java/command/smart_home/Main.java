package command.smart_home;

public class Main {
    public static void main(String[] args) {
        Light light = new Light() {
            @Override
            public void on() {
                System.out.println("Turned light on");
            }

            @Override
            public void off() {
                System.out.println("Turned light off");
            }
        };

        Command loc = new LightOnCommand(light);

        SimpleRemoteControl src = new SimpleRemoteControl();
        src.setCommand(loc);
        src.buttonWasPressed();
        src.buttonWasPressed();
        src.buttonWasPressed();
    }
}
