package decorator.beverages;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MilkBeverageExtTest {
    @Test
    void testExtensionDecorator() {
        Beverage beverage = new MilkBeverageExt(new MilkBeverageExt(new MilkBeverageExt(new DarkRoast())));
        beverage.prepare();
        assertEquals((new DarkRoast()).cost() + 10*3, beverage.cost());
    }
}