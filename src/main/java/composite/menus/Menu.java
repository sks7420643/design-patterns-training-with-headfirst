package composite.menus;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu extends MenuComponent {
    ArrayList<MenuComponent> menuComponents;

    @Override
    void print() {
        Iterator<MenuComponent> menuIterator = menuComponents.iterator();
        while (menuIterator.hasNext()) {
            menuIterator.next().print();
        }
    }

    @Override
    String getTitle() {
        return null;
    }

    @Override
    String getDescription() {
        return null;
    }
}
