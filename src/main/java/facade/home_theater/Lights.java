package facade.home_theater;

public interface Lights {
    void on();
    void off();
}
