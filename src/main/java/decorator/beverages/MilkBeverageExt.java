package decorator.beverages;

public class MilkBeverageExt extends Beverage {
    Beverage b;

    public MilkBeverageExt(Beverage b) {
        this.b = b;
    }

    @Override
    double cost() {
        return b.cost() + 10;
    }

    @Override
    void prepare() {
        b.prepare();
        System.out.println("Put some milk");
    }
}
