package factory.pizzas.simple;

public class Pizzeria {

    public void orderPizza(String type) {
        SimplePizzaFactory spf = new SimplePizzaFactory();

        Pizza p = spf.createPizza(type);

        p.prepare();
        p.bake();
        p.cut();
        p.box();
    }

}
