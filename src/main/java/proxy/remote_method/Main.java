package proxy.remote_method;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class Main {
    public static void main(String[] args) {
        HelloRemote helloRemote;
        try {
            helloRemote = new HelloRemoteImpl("it is my secrets");
            Naming.rebind("hello", helloRemote);
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
