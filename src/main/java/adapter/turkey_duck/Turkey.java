package adapter.turkey_duck;

public interface Turkey {
    void gobble();
    void fly();
}
