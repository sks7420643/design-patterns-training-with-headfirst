package singleton.chocolate;

public final class ChocolateBoiler {
    private static ChocolateBoiler boiler;

    private ChocolateBoiler() {}

    public static synchronized ChocolateBoiler getInstance() {
        if (boiler == null) {
            //noinspection InstantiationOfUtilityClass
            boiler = new ChocolateBoiler();
        }
        return boiler;
    }
}
