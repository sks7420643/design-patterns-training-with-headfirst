package proxy.remote_method;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloRemoteImpl extends UnicastRemoteObject implements HelloRemote {
    private static final long SerialVersionUID = 5L;
    String secrets;

    public HelloRemoteImpl(String secrets) throws RemoteException {
        this.secrets = secrets;
    }

    @Override
    public String sayHello() {
        System.out.println("Hello the world!");
        return secrets;
    }
}
