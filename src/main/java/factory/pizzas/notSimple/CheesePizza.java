package factory.pizzas.notSimple;

public class CheesePizza implements Pizza {
    @Override
    public void prepare() {
        System.out.println("Prepare cheese");
    }

    @Override
    public void bake() {
        System.out.println("Start baking");
    }

    @Override
    public void cut() {
        System.out.println("Slice the pizza");
    }

    @Override
    public void box() {
        System.out.println("Box cheese pizza");
    }
}
