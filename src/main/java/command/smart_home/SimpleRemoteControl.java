package command.smart_home;

public class SimpleRemoteControl {
    Command cSlot;

    public SimpleRemoteControl() {
    }

    public void setCommand(Command c) {
        cSlot = c;
    }

    public void buttonWasPressed() {
        cSlot.execute();
    }
}
