package strategy.duck_tales;

import strategy.duck_tales.fly.FlyBehaviour;

public abstract class Duck {
    private FlyBehaviour flyBehaviour;

    public void swim() {
        System.out.println("Duck is swimming!");
    }

    public void fly() {
        flyBehaviour.fly();
    }

    public void setFlyBehaviour(FlyBehaviour flyBehaviour) {
        this.flyBehaviour = flyBehaviour;
    }
}
