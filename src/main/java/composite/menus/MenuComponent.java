package composite.menus;

public abstract class MenuComponent {
    abstract void print();
    abstract String getTitle();
    abstract String getDescription();
    public MenuComponent getChild(int i) {
        throw new UnsupportedOperationException();
    }

}
