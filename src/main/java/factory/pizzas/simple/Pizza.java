package factory.pizzas.simple;

public interface Pizza {
    void prepare();
    void bake();
    void cut();
    void box();
}
