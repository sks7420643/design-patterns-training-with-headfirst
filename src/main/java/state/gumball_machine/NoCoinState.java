package state.gumball_machine;

public class NoCoinState implements State {
    GumballMachine gumballMachine;

    public NoCoinState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void putCoin() {
        System.out.println("U put the coin!");
        gumballMachine.setState(gumballMachine.oneCoinState);
    }

    @Override
    public void getCoin() {
        System.out.println("There no coin inside");
    }

    @Override
    public void rollCrank() {
        System.out.println("There no coin  inside");
    }

    @Override
    public void dispense() {
        System.out.println("There no coin  inside");
    }
}
