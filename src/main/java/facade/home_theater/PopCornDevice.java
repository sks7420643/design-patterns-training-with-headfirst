package facade.home_theater;

public interface PopCornDevice {
    void startBaking();
    void stopBaking();
}
