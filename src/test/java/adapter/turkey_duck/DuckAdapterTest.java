package adapter.turkey_duck;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DuckAdapterTest {
    static Turkey turkeyLike;

    @BeforeAll
    static void prepare() {
        Duck duck = new KingDuck();
        turkeyLike = new DuckAdapter(duck);
    }

    @Test
    void testTurkeyGobble() {
        turkeyLike.gobble();
    }

    @Test
    void testTurkeyFly() {
        turkeyLike.fly();
    }
}