package factory.pizzas.simple;

public class UnknownPizza implements Pizza {
    @Override
    public void prepare() {
        System.out.println("Unknown prepare");
    }

    @Override
    public void bake() {
        System.out.println("Unknown bake");
    }

    @Override
    public void cut() {
        System.out.println("Unknown cut");
    }

    @Override
    public void box() {
        System.out.println("Unknown box");
    }
}
