package singleton.chocolate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChocolateBoilerTest {
    @Test
    void testInstanceIsOnlyOne() {
        assertEquals(ChocolateBoiler.getInstance(), ChocolateBoiler.getInstance());
    }
}