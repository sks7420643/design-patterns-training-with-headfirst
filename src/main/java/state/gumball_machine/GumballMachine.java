package state.gumball_machine;

public class GumballMachine implements State {
    NoCoinState noCoinState;
    OneCoinState oneCoinState;

    State state = new EmptyMachineState();
    int gumballsCount = 0;

    public GumballMachine(int gumballsCount) {
        noCoinState = new NoCoinState(this);
        oneCoinState = new OneCoinState(this);

        if (gumballsCount > 0) {
            this.gumballsCount = gumballsCount;
            state = noCoinState;
        }
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public void putCoin() {
        state.putCoin();
    }

    @Override
    public void getCoin() {
        state.getCoin();
    }

    @Override
    public void rollCrank() {
        state.rollCrank();
    }

    @Override
    public void dispense() {
        state.dispense();
    }
}
