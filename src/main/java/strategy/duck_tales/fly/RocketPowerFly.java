package strategy.duck_tales.fly;

public class RocketPowerFly implements FlyBehaviour {
    @Override
    public void fly() {
        System.out.println("Super-speed fly!");
    }
}
