package strategy.duck_tales;

import strategy.duck_tales.fly.FlyBehaviour;
import strategy.duck_tales.fly.NoFly;

public class DuckCall {
    private FlyBehaviour fb;

    public DuckCall() {
        fb = new NoFly();
    }

    public void mimicDuck() {
        fb.fly();
    }

    public void setFb(FlyBehaviour fb) {
        this.fb = fb;
    }
}
