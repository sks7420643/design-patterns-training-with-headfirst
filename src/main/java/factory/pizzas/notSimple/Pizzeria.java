package factory.pizzas.notSimple;

public abstract class Pizzeria {

    public void orderPizza(String type) {
        Pizza p = createPizza(type);

        p.prepare();
        p.bake();
        p.cut();
        p.box();
    }

    abstract Pizza createPizza(String type);

}
