package iterator.companies_merge;

import java.util.ArrayList;
import java.util.Iterator;

public class CompanyOne {
    ArrayList<MenuItem> menuItems;

    public Iterator<MenuItem> createIterator() {
        return new CompanyOneMenuIterator(menuItems);
    }
}
