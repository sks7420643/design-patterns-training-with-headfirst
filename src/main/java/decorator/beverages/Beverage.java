package decorator.beverages;

abstract class Beverage {
    String description;

    abstract double cost();
    abstract void prepare();
}
